<h1 align = "center"> :fast_forward: :video_game: The Monster Killer :video_game: :rewind: </h1>

## 🖥 Preview
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/122482206_1767105130110497_7340931360426634095_n.jpg?_nc_cat=106&ccb=2&_nc_sid=0debeb&_nc_ohc=3PiKDqKNcNYAX-XIXLu&_nc_oc=AQks-knAPsFUNv6y_0tG1G2mh4_EyOdnYS_i3oCVsO93PEaxFePUNYC2ODd-Bcv0WIA&_nc_ht=scontent.fbnu2-1.fna&oh=01b71ae8c671d0f5cb083d4838393b98&oe=5FB7C3A1" width = "700">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/122341160_1767105136777163_2505349233581762221_n.jpg?_nc_cat=100&ccb=2&_nc_sid=0debeb&_nc_ohc=oS-z2RNGjWUAX-IAb3P&_nc_ht=scontent.fbnu2-1.fna&oh=de6d636aec49cd8c60c1dc12528eb281&oe=5FB705C1" width = "700">
</p>

---

## 📖 About
<p>Click game created using Vue.js, where when clicking buttons the player duels against a monster. Created from the basics of Vue.js.</p>

---

## 🛠 Technologies used
- CSS
- HTML
- Javascript
- Vue.js

---


## 🚀 How to execute the project
#### Clone the repository
git clone https://gitlab.com/ecpieritz/monster-killer.git

#### Enter directory
`cd monster-killer`

#### Run the server
- right click on the `index.html` file
- click on `open with liveserver`

---
Developed with 💙 by Emilyn C. Pieritz
